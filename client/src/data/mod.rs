use std::sync::Arc;

#[derive(Debug)]
pub struct Data {
    pub games: Vec<Arc<Game>>,
}

impl Default for Data {
    fn default() -> Self {
        Self {
            games: vec![
                Arc::new(Game {
                    name: "Example".to_string(),
                    uuid: 0,
                    description: "
                        The classic puzzle-platformer battle royale roguelike deckbuilder!
                    "
                    .to_string(),
                    logo: '📜',
                    achievements: None,
                }),
                Arc::new(Game {
                    name: "Example II: Terror of Lorem Ipsum".to_string(),
                    uuid: 1,
                    description: "
                        The thrilling sequel to the cult classic <i>example</i> is here!
                        And this time, our hero's got a new nemesis!
                    "
                    .to_string(),
                    logo: '💣',
                    achievements: None,
                }),
            ],
        }
    }
}

#[derive(Debug)]
pub struct Game {
    pub uuid: u128,
    pub name: String,
    pub description: String,
    pub logo: char,
    pub achievements: Option<Vec<(bool, Achievement)>>,
}

#[derive(Debug)]
pub struct Achievement {
    pub uuid: u128,
    pub name: String,
    pub description: String,
    pub logo: char,
}

use std::path::PathBuf;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    color_eyre::install().expect("color-eyre failed to initialize");
    compile_sass().expect("Sass compile failed");
    env_logger::init();
    client::router::init().await
}

fn compile_sass() -> std::io::Result<()> {
    let sass_path = PathBuf::from("./sass");
    let sass_files = std::fs::read_dir(sass_path)?;

    for file in sass_files {
        let sass_path = file?
            .path()
            .to_str()
            .expect("Style path with invalid unicode.")
            .to_string();
        std::fs::create_dir_all("css")?;
        let css_path = sass_path.replace("scss", "css").replace("sass", "css");
        let css =
            grass::from_path(sass_path.clone(), &grass::Options::default()).expect("Invalid CSS");

        std::fs::write(css_path, css)?;
    }

    Ok(())
}

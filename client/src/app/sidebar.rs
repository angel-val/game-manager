use crate::*;

pub fn sidebar(data: &web::Data<data::Data>) -> Markup {
    html! {
        @for game in &data.games {
            button hx-get=(format!("/pages/game/{}", game.uuid)) hx-target="#page" {
                p.logo { (game.logo) } p { (game.name) }
            }
        }
    }
}

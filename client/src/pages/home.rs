use crate::*;

#[get("/pages/home")]
pub async fn home() -> Markup {
    html! {
        h1 { "Welcome!" }
        p { "Browse games on the left." }
    }
}

use crate::*;

#[rustfmt::skip]
#[allow(clippy::future_not_send)]
pub async fn init() -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())

            .app_data(actix_web::web::Data::new(data::Data::default()))

            .service(actix_files::Files::new("css", "./css"))
            .service(actix_files::Files::new("static", "./static"))

            .service(app::app)

            .service(pages::home::home)
            .service(pages::game::game)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}

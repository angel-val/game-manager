use crate::*;

#[rustfmt::skip]
#[allow(clippy::future_not_send)]
pub async fn init() -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())
    })
    .bind(("127.0.0.1", 8282))?
    .run()
    .await
}

pub mod app;
pub mod data;
pub mod pages;
pub mod router;

pub use actix_web::*;
pub use maud::*;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

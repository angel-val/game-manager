#[actix_web::main]
async fn main() -> std::io::Result<()> {
    color_eyre::install().expect("color-eyre failed to initialize");
    env_logger::init();
    server::router::init().await
}

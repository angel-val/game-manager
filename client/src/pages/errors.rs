use crate::*;

pub fn error_404() -> Markup {
    html! {
        h1 { "Error 404" }
        p {
            "Resource not found. "
            a hx-get="/pages/home" hx-target="#page" { "Go home!" }
        }
    }
}

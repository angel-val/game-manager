use crate::*;

pub mod header_footer;
pub mod sidebar;

#[get("/")]
pub async fn app(data: web::Data<data::Data>) -> Markup {
    html! {
        (DOCTYPE)
        head {
            meta charset="utf-8";
            meta name="viewport" content="width=device-width, initial-scale=1";
            link rel="stylesheet" href="/css/reset.css";
            link rel="stylesheet" href="/css/main.css";
            link rel="icon" type="image/x-icon" href="/images/media/pfp.png";
            script src="https://unpkg.com/htmx.org@1.9.11" {}
            title { "game-manager" }
        }
        body {
            (header_footer::header())
            section #main {
                #sidebar { (sidebar::sidebar(&data)) }
                #page hx-get="/pages/home" hx-trigger="load" {}
            }
            (header_footer::footer())
        }
    }
}

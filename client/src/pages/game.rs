use crate::*;

#[get("/pages/game/{uuid}")]
pub async fn game(data: web::Data<data::Data>, uuid: web::Path<String>) -> Markup {
    let matching = &data
        .games
        .par_iter()
        .filter(|game| game.uuid.to_string() == *uuid)
        .collect::<Vec<_>>();
    let selected_game = matching.first();

    selected_game.map_or_else(
        || {
            html! {
                h1 { "Error 404" }
                p {
                    "Game not found. "
                    a hx-get="/pages/home" hx-target="#page" { "Go home!" }
                }
            }
        },
        |game| {
            html! {
                h1.logo { (game.logo) }
                h1 { (game.name) }
                p { (maud::PreEscaped(&game.description)) }
            }
        },
    )
}

use crate::*;

pub fn header() -> Markup {
    html! {
        header.red-black-box {
            button hx-get="/pages/home" hx-target="#page" { h2 { "BROWSE" } }
            button hx-get="/pages/home" hx-target="#page" { h2 { "CURATE" } }
        }
    }
}

pub fn footer() -> Markup {
    html! {
        footer.red-black-box {
            p {
                a href="https://gitlab.com/angel-val/game-manager/" target="_blank" {
                    image src="/static/images/icons/gitlab.svg" style="height: 0.75em; position: relative; top: 0.1em;";
                    " gitlab repo for this app"
                }
            }
        }
    }
}
